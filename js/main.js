var adad = "",
    ans = 0,
    operatorClicked = false,
    operator = "",
    Memory = 0;
$(() => {
    $('.num').click(e => {
        if (operatorClicked) adad = "";
        if (adad.length < 9 || (adad.length < 10 && adad.includes(".")))
            if (e.currentTarget.innerHTML != "." || !adad.includes("."))
                adad += e.currentTarget.innerHTML;
        NumPrint();
    });
    $('.operator').click(e => {
        if (!operatorClicked || e.currentTarget.innerHTML == "=" || e.currentTarget.innerHTML == operator)
            OperatorActions();
        operator = e.currentTarget.innerHTML;
        operatorClicked = true;
    });
    $('#BK').click(e => {
        adad = adad.length > 0 ? adad.substring(0, adad.length - 1) : adad;
        NumPrint();
    });
    $('#MC').click(e => {
        Memory = 0;
    });
    $('#MR').click(e => {
        adad = Memory.toString();
        NumPrint();
    });
    $('#MS').click(e => {
        if (operatorClicked) Memory = ans;
        else Memory = Number(adad);
    });
});

function NumPrint() {
    $('action')[0].innerHTML = adad.length > 0 ? adad : "0";
    operatorClicked = false;
}

function OperatorActions() {
    if (operator == "+") ans += Number(adad);
    if (operator == "-") ans -= Number(adad);
    if (operator == "*") ans *= Number(adad);
    if (operator == "/") ans /= Number(adad);
    if (operator == "") ans = Number(adad);
    ShowAns();
}

function ShowAns() {
    var s_ans = ans.toString();
    if (s_ans.length > 10 && s_ans.includes("."))
        if (ans > 0 && ans < 1)
            s_ans = ans.toPrecision(8).toString();
        else
            s_ans = ans.toPrecision(9).toString();
    else if (s_ans.length > 10)
        s_ans = ans.toPrecision(5).toString();
    $('result')[0].innerHTML = s_ans;
}